//
//  AppDelegate.m
//  Arch The Way
//
//  Created by Abhishek Pednekar on 19/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import "AppDelegate.h"
#import "GetPushDataService.h"
#import "Constants.h"
#import "CommonDigest.h"


@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize dic;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge)];
    
    dic = [[NSMutableDictionary alloc] init];
    
    [self configureRestKit];
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"LoginResponse"] length]==0)
    {
        
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        UITabBarController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"tabbarcontroller"];
        [tabBarController setViewControllers:[NSArray arrayWithObjects:
                                              [storyboard instantiateViewControllerWithIdentifier:@"HomeNav"],
                                              [storyboard instantiateViewControllerWithIdentifier:@"ClassesNav"],
                                              [storyboard instantiateViewControllerWithIdentifier:@"HistoryNav"],
                                              [storyboard instantiateViewControllerWithIdentifier:@"SettingsNav"],nil]];
        
        //    tabBarController.tabBar.tintColor=[UIColor cyanColor];//colorWithRed:88/255 green:172/225 blue:225/255 alpha:1.0];
        
        AppDelegate *appObj = (AppDelegate *)[UIApplication sharedApplication].delegate;
        tabBarController.selectedIndex = 0;

        appObj.window.rootViewController = tabBarController;
    }
    

    self.window.frame = [UIScreen mainScreen].bounds;

    return YES;
}


-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *deviceTokenString = [deviceToken description];
    deviceTokenString = [deviceTokenString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[NSUserDefaults standardUserDefaults] setObject:deviceTokenString forKey:@"DeviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Did Receive Remote Notification");
    //    [[[UIAlertView alloc] initWithTitle:@"Push Received" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    BOOL isRegistered = [[NSUserDefaults standardUserDefaults] boolForKey:@"isRegistered"];
    if (isRegistered) {
        //GetPushDataService *getPushDataService = [GetPushDataService shareGetPushDataService];
        //        @synchronized(getPushDataService)
        //        {
        //[getPushDataService getAllMessagesAndPersistCoreData];
        //        }
    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)configureRestKit
{
    NSURL *baseURL = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceToken"];
    
    if (deviceToken == nil)
    {
        deviceToken = @"1";
    }
    
    NSString *UUID = [[NSUUID UUID] UUIDString];
    [[NSUserDefaults standardUserDefaults] setObject:UUID forKey:@"UUID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:UUID,@"unique_id",deviceToken,@"push_id", nil];
    
    [client postPath:REGISTERFORPUSH_NOTY parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Request Successful, response '%@'", responseStr);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

+ (NSString *)stringWithMD5Hash:(NSString *)inStr {
    const char *cStr = [inStr UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), result );
    return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15] ];
}

@end
