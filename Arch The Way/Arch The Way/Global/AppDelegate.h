//
//  AppDelegate.h
//  Arch The Way
//
//  Created by Abhishek Pednekar on 19/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(nonatomic,strong)NSMutableDictionary *dic;
@property (strong, nonatomic) UIWindow *window;

+(NSString *)stringWithMD5Hash:(NSString *)inStr;

@end

