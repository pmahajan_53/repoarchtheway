//
//  RegisterCell.h
//  Arch The Way
//
//  Created by Abhishek Pednekar on 21/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnTick;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;

@end
