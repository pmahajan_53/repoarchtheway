//
//  EmailCell.h
//  Arch The Way
//
//  Created by Abhishek Pednekar on 21/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *txtEmailId;

@end
