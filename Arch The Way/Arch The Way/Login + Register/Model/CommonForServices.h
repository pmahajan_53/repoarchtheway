//
//  CommonForServices.h
//  Arch The Way
//
//  Created by Amit Naik on 24/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface CommonForServices : NSObject

+(void)registerTeacher:(NSDictionary *)dictionary;
+(void)nlogin_teacher:(NSDictionary *)dictionary;
+(BOOL) validateEmail:(NSString*) emailString;
+(NSString *)isString:(NSString *)string;
+(void)verifyMethod:(NSDictionary *)dictionary;
+(void)sendSmsToUsesr:(NSString *)url;
+(void)messageSendingAPI:(NSDictionary *)dictionary;
+(void)uploadProfilePicture:(NSData *)imageData;
+(void)sendScheduleAPI:(NSString *)dictionary;



+(NSDictionary*)dictionaryFromOAuthResponseString:(NSString*)response;

@end
