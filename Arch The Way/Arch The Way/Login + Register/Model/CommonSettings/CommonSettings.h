//
//  CommonSettings.h
//  SipSnapp
//
//  Created by NeoSoft on 23/05/14.
//  Copyright (c) 2014 SS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface CommonSettings : NSObject

@property(nonatomic, strong) NSString *storyboardName;
//@property (nonatomic ,strong) UIStoryboard *storyboardMain;
@property (nonatomic) BOOL isfacebookSharingOn;
@property (nonatomic) BOOL isTwiteerSharingOn;
@property (nonatomic) BOOL isRateBeerSharingOn;
@property (nonatomic) BOOL isEmailSharingOn;
@property (nonatomic) BOOL isSearching;

//@property(nonatomic, strong) UIAlertView *noInternetAlert;

+(CommonSettings*) sharedInstance;
-(void) resetContents;

-(void) showHUD;
-(void) hideHUD;

-(void) showActivityIndicator;
-(void) hideActivityIndicator;
//-(UIFont *)RokkittBold:(float)size;
//-(UIFont *)RokkittRegular:(float)size;

- (void)showAlertWithTitle:(NSString*)title message:(NSString*)message;
- (void)showNetWorkAlert;
-(BOOL)CheckNetWorkConnection;

-(float)iOSVersion;
-(NSString *)iPhoneDevice;

@end
