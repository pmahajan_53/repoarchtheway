//
//  CommonSettings.m
//  SipSnapp
//
//  Created by NeoSoft on 23/05/14.
//  Copyright (c) 2014 SS. All rights reserved.
//

#import "CommonSettings.h"
#import "AppDelegate.h"

#define ROKKITTBOLD @"Rokkitt-Bold"
#define ROKKITTREGULAR @"Rokkitt-Regular"

@interface CommonSettings ()
@property (atomic, strong) UIActivityIndicatorView* indicator;

@end

@implementation CommonSettings
@synthesize storyboardName;
@synthesize isfacebookSharingOn,isRateBeerSharingOn,isTwiteerSharingOn,isEmailSharingOn;


static CommonSettings *sharedInstance;

+(CommonSettings*) sharedInstance
{
    if (!sharedInstance)
    {
        sharedInstance = [[CommonSettings alloc] init];
        [sharedInstance resetContents];
    }
    
    return sharedInstance;
}

//-(void) resetContents
//{
//    self.storyboardName = nil;
//    self.storyboardMain = nil;
//    
//    if (self.progressView) {
//        [self.progressView hide:NO];
//    }
//    if ([self.noInternetAlert isVisible]) {
//        [self.noInternetAlert dismissWithClickedButtonIndex:0 animated:YES];
//    }
//}
//
//-(void) showHUD
//{
//    if (self.progressView)
//    {
//        [self.progressView hide:NO];
//    }
//    self.progressView = [MBProgressHUD showHUDAddedTo:((AppDelegate*)[[UIApplication sharedApplication] delegate]).window animated:YES];
//   // [self.progressView setColor:[UIColor clearColor]];
//
//    self.progressView.animationType = MBProgressHUDAnimationFade;
//    self.progressView.mode = MBProgressHUDModeIndeterminate;
//    self.progressView.removeFromSuperViewOnHide = YES;
//}
//
//-(void) hideHUD
//{
//    [self.progressView hide:YES];
//}
//
//-(void) showActivityIndicator
//{
//    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    self.indicator.color = [UIColor colorWithRed:87.0f/255.0f green:96.0f/255.0f blue:25.0f/255.0f alpha:1.0f];
//    self.indicator.center = CGPointMake(((AppDelegate*)[[UIApplication sharedApplication] delegate]).window.center.x, 180);
//    
//    [self.indicator startAnimating];
//    [((AppDelegate*)[[UIApplication sharedApplication] delegate]).window addSubview:self.indicator];
//    
//}

-(void) hideActivityIndicator
{
    for (UIActivityIndicatorView *activity in [((AppDelegate*)[[UIApplication sharedApplication] delegate]).window subviews])
    {
        if ([activity isKindOfClass:[UIActivityIndicatorView class]])
        {
            [activity stopAnimating];
            activity.hidden = YES;
            [activity removeFromSuperview];
            break;
        }
    }
}

-(UIFont *)RokkittBold:(float)size
{
    return [UIFont fontWithName:ROKKITTBOLD size:size];
}
-(UIFont *)RokkittRegular:(float)size
{
    return [UIFont fontWithName:ROKKITTBOLD size:size];
}


-(BOOL)CheckNetWorkConnection
{
	BOOL check;
	Reachability *Reach = [Reachability reachabilityForInternetConnection];
	[Reach startNotifier];
	NetworkStatus netStatus = [Reach currentReachabilityStatus];
	if(netStatus == NotReachable)
	{
		check=NO;
	}
	else
	{
		check=YES;
	}
	return check;
}

#pragma mark - Alert View

- (void)showAlertWithTitle:(NSString*)title message:(NSString*)message
{
    //--- to show alert ----
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title message: message delegate: NULL cancelButtonTitle: @"OK" otherButtonTitles: NULL];
	[alert show];
}

- (void)showNetWorkAlert
{
    //--- to show network alert ----
    UIAlertView* progressAlert= [[UIAlertView alloc] initWithTitle:@"No Internet !" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
	[progressAlert show];
}


#pragma mark - iOS version Check
-(float)iOSVersion
{
    return [[[UIDevice currentDevice] systemVersion] floatValue];
}


#pragma mark - iPhone Check
-(NSString *)iPhoneDevice
{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        return @"4";
    }
    if(result.height == 568)
    {
        return @"5";
    }
    if(result.height == 667)
    {
        return @"6";
    }
    if(result.height == 736)
    {
        return @"6+";
    }
    return @"";
}
@end
