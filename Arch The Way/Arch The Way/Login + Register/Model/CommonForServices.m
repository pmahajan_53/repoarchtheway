//
//  CommonForServices.m
//  Arch The Way
//
//  Created by Amit Naik on 24/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import "CommonForServices.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "SBJsonParser.h"
#import "SVProgressHUD.h"

@implementation CommonForServices


+(void)registerTeacher:(NSDictionary *)dictionary
{
    NSURL *baseURL = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    
    [client postPath:REGISTERURL parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (operation.response.statusCode == 200)
        {
            NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            
            if ([responseStr isEqualToString:@"This email_id is already in use."])
            {
                [SVProgressHUD dismiss];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"This email_id is already in use." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:responseStr forKey:@"RegisterResponse"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSLog(@"Request Successful, response '%@'", responseStr);
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
}

+(void)nlogin_teacher:(NSDictionary *)dictionary
{
    NSURL *baseURL = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    
    [client postPath:LOGINURL parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%ld",(long)operation.response.statusCode);
        
        if (operation.response.statusCode == 200)
        {
            id responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            
            if ([responseStr isEqualToString:@"Incorrect username or password"])
            {
                [SVProgressHUD dismiss];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@" Incorrect username or password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:responseStr forKey:@"LoginResponse"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSLog(@"Request Successful, response '%@'", responseStr);
                
                NSLog(@"Response is %@",[responseStr class]);
                
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                UITabBarController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"tabbarcontroller"];
                [tabBarController setViewControllers:[NSArray arrayWithObjects:
                                                      [storyboard instantiateViewControllerWithIdentifier:@"HomeNav"],
                                                      [storyboard instantiateViewControllerWithIdentifier:@"ClassesNav"],
                                                      [storyboard instantiateViewControllerWithIdentifier:@"HistoryNav"],
                                                      [storyboard instantiateViewControllerWithIdentifier:@"SettingsNav"],nil]];
                
                //    tabBarController.tabBar.tintColor=[UIColor cyanColor];//colorWithRed:88/255 green:172/225 blue:225/255 alpha:1.0];
                
                AppDelegate *appObj = (AppDelegate *)[UIApplication sharedApplication].delegate;
                tabBarController.selectedIndex = 0;
                appObj.window.rootViewController = tabBarController;
            }
        }
        else
        {
            [SVProgressHUD dismiss];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [SVProgressHUD dismiss];
    }];
}

+(void)verifyMethod:(NSDictionary *)dictionary
{
    NSURL *baseURL = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    
    [client postPath:VERIFYURL parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (operation.response.statusCode == 200)
        {
            NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            UITabBarController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"tabbarcontroller"];
            [tabBarController setViewControllers:[NSArray arrayWithObjects:
                                                  [storyboard instantiateViewControllerWithIdentifier:@"HomeNav"],
                                                  [storyboard instantiateViewControllerWithIdentifier:@"ClassesNav"],
                                                  [storyboard instantiateViewControllerWithIdentifier:@"HistoryNav"],
                                                  [storyboard instantiateViewControllerWithIdentifier:@"SettingsNav"],nil]];
            
            AppDelegate *appObj = (AppDelegate *)[UIApplication sharedApplication].delegate;
            tabBarController.selectedIndex = 0;
            appObj.window.rootViewController = tabBarController;
        }
        else
        {
            [SVProgressHUD dismiss];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [SVProgressHUD dismiss];
    }];
}

+(void)sendSmsToUsesr:(NSString *)url
{
    NSURL *baseURL = [NSURL URLWithString:url];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    
    [client postPath:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        NSLog(@"Request Successful, response '%@'", responseStr);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];
}

+(void)messageSendingAPI:(NSDictionary *)dictionary
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:FEEDBACK_URL]];
    //NSData *parameterData = [dictionary dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //[request setHTTPBody:parameterData];
    [request setHTTPMethod:@"GET"];
    [request addValue: @"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSLog(@"requestReply: %@", requestReply);
    }] resume];
    
    
    
    
    /*NSURL *baseURL = [NSURL URLWithString:BASE_URL];
    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    
    [client getPath:FEEDBACK_URL parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [SVProgressHUD dismiss];
        if (operation.response.statusCode == 200)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Posted Successfully !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        
        NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"Request Successful, response '%@'", responseStr);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
        NSLog(@"[HTTPClient Error]: %@", error.localizedDescription);
    }];*/
}

+(void)sendScheduleAPI:(NSString *)dictionary
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:FEEDBACK_URL]];
    //NSData *parameterData = [dictionary dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //[request setHTTPBody:parameterData];
    [request setHTTPMethod:@"GET"];
    [request addValue: @"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *requestReply = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        NSLog(@"requestReply: %@", requestReply);
    }] resume];

    
    
    /*NSString *requestUrl = [NSString stringWithFormat:@"%@",FEEDBACK_URL];
    
    id response = [self serverCallAndResponse:requestUrl dictionary:dictionary method:@"POST"];
    
    [SVProgressHUD dismiss];
    if ([response isKindOfClass:[NSDictionary class]])
    {
        if ([[response objectForKey:@"response"] isEqualToString:@"success"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Posted Successfully !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"PostedSucessfully" object:self];
        }
    }*/
}

+(id)serverCallAndResponse:(NSString *)requestUrl dictionary:(NSString *)dictionary method:(NSString *)method
{
    NSHTTPURLResponse *res = nil;
    NSError *err = nil;
    requestUrl=[requestUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url=[NSURL URLWithString:requestUrl];
    NSData *postData = [NSData dataWithBytes:[dictionary UTF8String] length:[dictionary length]];
    
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    [req setTimeoutInterval:20.0];
    [req setHTTPMethod:method];
    [req setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    [req setHTTPBody:postData];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:req returningResponse:&res error:&err];
    
    NSLog(@"Response - %@",res);
    id responseDict1 = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"Response - %@",responseDict1);
    
    return responseDict1;
}

+(void)uploadProfilePicture:(NSData *)imageData
{
    NSHTTPURLResponse *res = nil;
    NSError *err = nil;
    
    NSString *filename = @"multipartFile";
    NSMutableURLRequest *req= [[NSMutableURLRequest alloc] init];
    [req setURL:[NSURL URLWithString:PHOTO_URL]];
    [req setTimeoutInterval:60.0];
    [req setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [req addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"multipartFile\"; filename=\"%@.png\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[NSData dataWithData:imageData]];
    [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [req setHTTPBody:postbody];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:req returningResponse:&res error:&err];
    
    NSLog(@"res - %@",res);
    
    NSLog(@"res - %ld",(long)res.statusCode);
    
    id serviceResponse=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
}

+(BOOL) validateEmail:(NSString*) emailString
{
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}

+(NSString *)isString:(NSString *)string
{
    if ([string isKindOfClass:[NSNull class]] || [string isEqualToString:nil] || [string isEqualToString:NULL] || [string isEqual:[NSNull null]] || string.length == 0)
    {
        return @"";
    }
    return string;
}

+(NSDictionary*)dictionaryFromOAuthResponseString:(NSString*)response
{
    NSArray *tokens = [response componentsSeparatedByString:@","];
    NSMutableArray *ar = [[NSMutableArray alloc] init];
    NSMutableArray *ar1 = [[NSMutableArray alloc] init];
    NSMutableDictionary *oauthDict = [[NSMutableDictionary alloc] initWithCapacity:5];
    
    for(NSString *t in tokens) {
        NSArray *entry = [t componentsSeparatedByString:@":"];
        NSString *key = entry[0];
        NSString *val;
        if ([key isEqualToString:@"batch_code"])
        {
            [ar addObject:entry[1]];
            val = [ar componentsJoinedByString:@","];
        }
        else if ([key isEqualToString:@"title"])
        {
            [ar1 addObject:entry[1]];
            val = [ar1 componentsJoinedByString:@","];
        }
        else
        {
            val = entry[1];
        }
        
        
        [oauthDict setValue:val forKey:key];
    }
    
    return [NSDictionary dictionaryWithDictionary:oauthDict];
}




@end
