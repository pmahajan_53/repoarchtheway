//
//  LoginClass.h
//  Arch The Way
//
//  Created by Amit Naik on 01/07/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface LoginClass : RKObjectManager

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *emailId;

@end
