//
//  Constants.h
//  ArchTheWay
//
//  Created by Piyush Jain on 31/05/14.
//  Copyright (c) 2014 Piyush Jain. All rights reserved.
//

#define NOTIFICATION_DOWNLOADSTARTED @"DownloadStarted"
#define NOTIFICATION_DOWNLOADFAILED @"DownloadFailed"
#define NOTIFICATION_RELOADDATA @"RELOADATA"
#define NOTIFICATION_DOWNLOADUSERCANCELLED @"UserCancelled"

//#define BASE_URL    @"http://archtheway.com/testing/mobile/iphone/student" //testing
//#define BASE_URL    @"http://archtheway.com/mobile/iphone/student"  //production

#define BASE_URL    @"http://archtheway.com/testing/mobile/iphone/teacher"

#define SINGLESUBS    [NSString stringWithFormat:@"%@/push_notification/single_subscription.php",BASE_URL]
#define SINGLEUNSUB   [NSString stringWithFormat:@"%@/unsubscribe_batch.php",BASE_URL]
#define UBCSUBMISSION [NSString stringWithFormat:@"%@/push_notification/multiple_subcription.php",BASE_URL]
//#define PHOTO_URL     [NSString stringWithFormat:@"%@uploads/",BASE_URL]
#define MESSAGES_URL  [NSString stringWithFormat:@"%@/sync_messages.php",BASE_URL]
//#define FEEDBACK_URL  [NSString stringWithFormat:@"%@/push_notification/send_feedback.php",BASE_URL]
#define MESSAGE_DELIVER_URL [NSString stringWithFormat:@"%@/message_deliver.php",BASE_URL]
//message_deliver.php
#define PUSHNOTIFICATION [NSString stringWithFormat:@"%@/push_notification/push_message.php",BASE_URL]

#define SENDPUSHNOTIFICATION_SUBS [NSString stringWithFormat:@"%@/push_notification/send_subscriber_push.php",BASE_URL]


#define REGISTERFORPUSH_NOTY    [NSString stringWithFormat:@"%@/push_notification/register_device.php",BASE_URL]
#define REGISTERURL   [NSString stringWithFormat:@"%@/register_teacher.php",BASE_URL]
#define LOGINURL   [NSString stringWithFormat:@"%@/nlogin_teacher.php",BASE_URL]
#define VERIFYURL   [NSString stringWithFormat:@"%@/verify_code.php",BASE_URL]
#define FEEDBACK_URL  [NSString stringWithFormat:@"%@/push_notification/nfuture_send_message.php",BASE_URL]
#define PHOTO_URL     [NSString stringWithFormat:@"%@/push_notification/upload_file.php",BASE_URL]
#define SEND_SCHEDULE  [NSString stringWithFormat:@"%@/push_notification/nfuture_send_message.php",BASE_URL]


#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

//http://www.archtheway.com/testing/mobile/iphone/student/push_notification/delete_batches.php
#define DELETE_BATCHES [NSString stringWithFormat:@"%@/push_notification/delete_batches.php",BASE_URL]

#define RETREIVE_BATCHES [NSString stringWithFormat:@"%@/push_notification/restore_batches.php",BASE_URL]

#define kKeyVendor @"ArchTheWay"
