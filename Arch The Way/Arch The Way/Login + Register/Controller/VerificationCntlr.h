//
//  VerificationCntlr.h
//  Arch The Way
//
//  Created by Abhishek Pednekar on 26/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerificationCntlr : UIViewController
{
    __weak IBOutlet UITextField *txtMobileNo;
    
    __weak IBOutlet UITextField *txtConfirmationCode;
}
- (IBAction)btnConfirmClicked:(id)sender;
- (IBAction)btnResendCodeClicked:(id)sender;

- (IBAction)cancelBtnClicked:(id)sender;
@property (strong, nonatomic) UIWindow *window;

@end
