//
//  LoginCntlr.m
//  Arch The Way
//
//  Created by Abhishek Pednekar on 19/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import "LoginCntlr.h"
#import "SignUpCntlr.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "CommonForServices.h"
#import "SVProgressHUD.h"

@interface LoginCntlr ()

@end

@implementation LoginCntlr

- (void)viewDidLoad
{
    [super viewDidLoad];
    imgNavIcon.layer.cornerRadius = 5.0;
    [[imgNavIcon layer] setMasksToBounds:YES];
    _password.delegate = self;
    _emailId.delegate = self;
    
    // Do any additional setup after loading the vipasswordew.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (IBAction)btnSignUpClicked:(id)sender
{
    NSString *UUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"];
    
    NSString *passWord = [AppDelegate stringWithMD5Hash:@"Password"];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SignUpCntlr *signUpObj = [storyboard instantiateViewControllerWithIdentifier:@"SignUpCntlr"];
    [self presentViewController:signUpObj animated:YES completion:nil];
}


- (IBAction)btnForgotPswdClicked:(id)sender
{

}
- (IBAction)btnSignInClicked:(id)sender
{
    if ([_emailId.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your email id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if(![self emailCheck:_emailId])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Invalid Email Id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
//    if ([CommonForServices validateEmail:_emailId.text])
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"This email_id is already in use." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//        return;
//    }
     if ([_password.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    [SVProgressHUD show];
    NSString *UUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"];
    
    NSString *passWord = [AppDelegate stringWithMD5Hash:_password.text];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:UUID,@"unique_id",@"1",@"push_id",@"1.0",@"version",_emailId.text,@"username",passWord,@"password", nil];
    
    [CommonForServices nlogin_teacher:dictionary];
    
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Email Id

-(BOOL)emailCheck:(UITextField*)sender
{
        NSString* text = [sender.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        BOOL stricterFilter = YES;
        NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
        NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        return [emailTest evaluateWithObject:text];
}

@end
