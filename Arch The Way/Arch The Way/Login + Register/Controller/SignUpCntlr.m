//
//  SignUpCntlr.m
//  Arch The Way
//
//  Created by Abhishek Pednekar on 21/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import "SignUpCntlr.h"
#import "NameCell.h"
#import "EmailCell.h"
#import "DisplayNameCell.h"
#import "NewPasswordCell.h"
#import "ConfirmPasswordCell.h"
#import "PhoneCell.h"
#import "AboutMeCell.h"
#import "TimeZoneCell.h"
#import "RegisterCell.h"
#import "CommonForServices.h"
#import "AppDelegate.h"
#import "VerificationCntlr.h"
#import "SVProgressHUD.h"
#import "TimeZoneCntlr.h"

@interface SignUpCntlr ()

@end

@implementation SignUpCntlr

- (void)viewDidLoad
{
    [super viewDidLoad];
    //KEYBOARD NOTIFICATION
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    

    isSelected = NO;
    agreedToTerms = NO;
    
    strTimezone = @"";
    pickerTimezones = [[UIPickerView alloc] init];
    pickerTimezones.dataSource = self;
    pickerTimezones.delegate = self;
    
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(toolBarDoneClicked:)];
    
    
    UIBarButtonItem* flexSpace = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *fake = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil] ;
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects: fake,flexSpace,fake,doneButton,nil] animated:YES];

    

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [tblView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Tableview Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        return 85;
    }
    if(indexPath.row == 1)
    {
        return 60;
    }
    if(indexPath.row == 2)
    {
        return 75;
    }
    if(indexPath.row == 3)
    {
        return 60;
    }
    if(indexPath.row == 4)
    {
        return 60;
    }
    if(indexPath.row == 5)
    {
        return 60;
    }
    if(indexPath.row == 6)
    {
        return 150;
    }
    if(indexPath.row == 7)
    {
        return 60;
    }
    if(indexPath.row == 8)
    {
        return 100;
    }
    
    return 60;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        static NSString *cellIdentifier = @"NameCell";
        NameCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[NameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.txtFirstName.delegate = self;
        cell.txtLastName.delegate = self;

        cell.txtFirstName.tag = 0;
        cell.txtLastName.tag = 1;
        
        [cell.btnImg addTarget:self action:@selector(btnImgClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if (buttonClicked)
        {
            cell.imgProfilePic.image = imgForButton;
        }
        else
        {
            cell.imgProfilePic.image = [UIImage imageNamed:@"dummyDP"];
        }
        return cell;
    }
    
    if (indexPath.row == 1)
    {
        static NSString *cellIdentifier = @"EmailCell";
        EmailCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[EmailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.txtEmailId.delegate = self;

        cell.txtEmailId.tag = 2;
        return cell;
    }
    if (indexPath.row == 2)
    {
        static NSString *cellIdentifier = @"DisplayNameCell";
        DisplayNameCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[DisplayNameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.txtDisplayName.delegate = self;

        cell.txtDisplayName.tag = 3;
        return cell;
    }
    if (indexPath.row == 3)
    {
        static NSString *cellIdentifier = @"NewPasswordCell";
        NewPasswordCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[NewPasswordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.txtPassword.delegate = self;

        cell.txtPassword.tag = 4;
        return cell;
    }
    if (indexPath.row == 4)
    {
        static NSString *cellIdentifier = @"ConfirmPasswordCell";
        ConfirmPasswordCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[ConfirmPasswordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.txtConfirmPassord.delegate = self;

        cell.txtConfirmPassord.tag = 5;
        return cell;
    }
    if (indexPath.row == 5)
    {
        static NSString *cellIdentifier = @"PhoneCell";
        PhoneCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[PhoneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.txtPhoneNumber.delegate = self;
        cell.txtPhoneNumber.tag = 6;
        return cell;
    }
    if (indexPath.row == 6)
    {
        static NSString *cellIdentifier = @"AboutMeCell";
        AboutMeCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[AboutMeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        cell.txtViewAbtMe.delegate = self;
        cell.txtViewAbtMe.tag = 7;
        return cell;
    }
    if (indexPath.row == 7)
    {
        static NSString *cellIdentifier = @"TimeZoneCell";
        TimeZoneCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[TimeZoneCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        
        strTimezone = [pref objectForKey:@"TimeZone"];
        cell.txtFldTimeZone.text = strTimezone;
        cell.txtFldTimeZone.delegate = self;
        cell.txtFldTimeZone.tag = 8;
        
        return cell;
    }
    if (indexPath.row == 8)
    {
        static NSString *cellIdentifier = @"RegisterCell";
        RegisterCell *cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            cell = [[RegisterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        [cell.btnRegister addTarget:self action:@selector(registerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        if (agreedToTerms)
        {
            [cell.btnTick setBackgroundImage:[UIImage imageNamed:@"CheckYes"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.btnTick setBackgroundImage:[UIImage imageNamed:@"CheckNo"] forState:UIControlStateNormal];
        }
        [cell.btnTick addTarget:self action:@selector(btnAgreeTerms:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    return nil;
}

#pragma mark - TextField Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    reloadTo = (int)textField.tag;
    if(textField.tag == 8)
    {
        TimeZoneCntlr *timeZoneObj = [self.storyboard instantiateViewControllerWithIdentifier:@"TimeZoneCntlr"];
        [self presentViewController:timeZoneObj animated:YES completion:nil];
        
    }
    return YES;
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    reloadTo = (int)textView.tag;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField.tag == 0)
    {
        strFirstName = textField.text;
    }
    if(textField.tag == 1)
    {
        strLastName = textField.text;
    }
    if(textField.tag == 2)
    {
        strEmailId = textField.text;
    }
    if(textField.tag == 3)
    {
        strDisplayName = textField.text;
    }
    if(textField.tag == 4)
    {
        strNewPswd = textField.text;
    }
    if(textField.tag == 5)
    {
        strConfirmPwsd = textField.text;
    }
    if(textField.tag == 6)
    {
        strPhnNumber = textField.text;
    }
    if(textField.tag == 8)
    {
        //strTimezone = textField.text;
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView.tag == 7)
    {
        strAbtMe = textView.text;
    }
}

-(IBAction)btnAgreeTerms:(id)sender
{
    if (agreedToTerms)
    {
        agreedToTerms = NO;
    }
    else
    {
        agreedToTerms = YES;
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:8 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [tblView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];

}

#pragma mark - 
#pragma mark - Register Button

-(IBAction)registerBtnClick:(id)sender
{
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//    
//    VerificationCntlr *verificationObj = [storyboard instantiateViewControllerWithIdentifier:@"VerificationCntlr"];
//    
//    [self presentViewController:verificationObj animated:YES completion:nil];
    

    NSLog(@"registerBtnClick");
    if ([strFirstName length]==0)
    {
        [self getAlertView:@"Error" message:@"Please enter your Full name"];
    }
    else if ([strLastName length]==0)
    {
        [self getAlertView:@"Error" message:@"Please enter your Last name"];
    }
    else if (![self validateEmail:strEmailId])
    {
        [self getAlertView:@"Error" message:@"Please enter valid Email Id"];
    }
    else if ([strDisplayName length]==0)
    {
        [self getAlertView:@"Error" message:@"Please enter the Display name"];
    }
    else if ([strNewPswd length]==0)
    {
        [self getAlertView:@"Error" message:@"Please enter your Password"];
    }
    else if (![strNewPswd isEqualToString:strConfirmPwsd])
    {
        [self getAlertView:@"Error" message:@"Passwords entered don't match"];
    }
    else if ([strPhnNumber length]!=10)
    {
        [self getAlertView:@"Error" message:@"Please enter valid phone number"];
    }
    else if ([strTimezone length]==0)
    {
        [self getAlertView:@"Error" message:@"Please select your timezone"];
    }
    else if (!agreedToTerms)
    {
        [self getAlertView:@"Error" message:@"Please agree to our terms and conditions"];
    }
        
    else
    {
        [SVProgressHUD show];
        NSString *UUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"];
        
        NSString *password = [AppDelegate stringWithMD5Hash:strNewPswd];

        
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:strFirstName,@"firstname",strLastName,@"lastname",strEmailId,@"email_id",password,@"password",strDisplayName,@"display_name",strPhnNumber,@"phone_number",strAbtMe,@"about_me",@"",@"profile_photo_filename",UUID,@"unique_id",@"1",@"push_id",@"1.0",@"version",@"234234",@"reset_code",strTimezone,@"timezone", nil];
        
        [CommonForServices registerTeacher:dictionary];
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 8.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
           
            [self changeTheView];
        });

        
        
    }
}

-(void)changeTheView
{
    id response = [[NSUserDefaults standardUserDefaults] objectForKey:@"RegisterResponse"];
    
    if ([[CommonForServices isString:response] isEqualToString:@""])
    {
        //Message
        
        return;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    VerificationCntlr *verificationObj = [storyboard instantiateViewControllerWithIdentifier:@"VerificationCntlr"];
    
    [self presentViewController:verificationObj animated:YES completion:nil];
    [SVProgressHUD dismiss];
}

#pragma mark - Picker Methods

//Columns in picker views

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}
//Rows in each Column

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return arrTimeZones.count;
}

-(NSString*) pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [arrTimeZones objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    strTimezone = [arrTimeZones objectAtIndex:row];
//    [tblView reloadData];
}

-(IBAction)toolBarDoneClicked:(UITextField *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:7 inSection:0];
    NSArray *indexPaths = [[NSArray alloc] initWithObjects:indexPath, nil];
    [tblView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}



#pragma mark - Alertview Methods

-(UIAlertView *)getAlertView:(NSString *)title message:(NSString*)Message
{
    UIAlertView *generalAlert = [[UIAlertView alloc] initWithTitle:title message:Message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [generalAlert show];
    return generalAlert;
}


#pragma mark - Keyboard Methods

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
    }
    
    NSNumber *rate = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:rate.floatValue animations:^{
        tblView.contentInset = contentInsets;
        tblView.scrollIndicatorInsets = contentInsets;
    }];
    if (reloadTo == 0 || reloadTo == 1)
    {
        [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    if (reloadTo == 2)
    {
        [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    if (reloadTo == 3)
    {
        [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    if (reloadTo == 4)
    {
        [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    if (reloadTo == 5)
    {
        [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    if (reloadTo == 6)
    {
        [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    if (reloadTo == 7)
    {
        [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }


    
//        [tblView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:5] atScrollPosition:UITableViewScrollPositionTop animated:YES];
   }

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSNumber *rate = notification.userInfo[UIKeyboardAnimationDurationUserInfoKey];
    [UIView animateWithDuration:rate.floatValue animations:^{
        tblView.contentInset = UIEdgeInsetsZero;
        tblView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }];
}

#pragma mark - Email validation

-(BOOL) validateEmail:(NSString*) emailString
{
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}

#pragma mark - Button Click

-(IBAction)btnImgClicked:(id)sender
{
    buttonClicked = YES;
    UIActionSheet* myAction = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose existing", nil];
    [myAction showInView:self.view];

}

#pragma mark - Image Picker Delegate & Related

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    imgForButton = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    NSData *data = UIImagePNGRepresentation(imgForButton);
    
    [CommonForServices uploadProfilePicture:data];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    buttonClicked = YES;
    
    NSIndexPath *tmpIndexpath=[NSIndexPath indexPathForRow:0 inSection:0];
    [tblView beginUpdates];
    [tblView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:tmpIndexpath, nil] withRowAnimation:UITableViewRowAnimationFade];
    [tblView endUpdates];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    if (buttonIndex==0)
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        }
        else
        {
            UIAlertView *noCameraAlert = [[UIAlertView alloc] initWithTitle:@"Sorry !" message:@"Camera Not Available." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [noCameraAlert show];
            
        }
    }
    else if (buttonIndex==1)
    {
        //picker.mediaTypes = [NSArray arrayWithObject:kUTTypeVideo];
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self presentViewController:picker animated:YES completion:nil];
    }
}


- (IBAction)cancelBtnClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
