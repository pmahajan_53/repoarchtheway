//
//  TimeZoneCntlr.h
//  Arch The Way
//
//  Created by Abhishek Pednekar on 11/07/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUpCntlr.h"

@interface TimeZoneCntlr : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    NSArray *arrTimeZones;
}
@end
