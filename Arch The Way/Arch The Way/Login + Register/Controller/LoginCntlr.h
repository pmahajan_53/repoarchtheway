//
//  LoginCntlr.h
//  Arch The Way
//
//  Created by Abhishek Pednekar on 19/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginCntlr : UIViewController <UITextFieldDelegate>
{
    __weak IBOutlet UIImageView *imgNavIcon;
}
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *emailId;
- (IBAction)btnSignInClicked:(id)sender;
- (IBAction)btnSignUpClicked:(id)sender;

- (IBAction)btnForgotPswdClicked:(id)sender;
@end
