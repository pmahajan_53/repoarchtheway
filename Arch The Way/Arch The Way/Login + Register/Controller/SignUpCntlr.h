//
//  SignUpCntlr.h
//  Arch The Way
//
//  Created by Abhishek Pednekar on 21/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeZoneCntlr.h"


@interface SignUpCntlr : UIViewController <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate>
{
    IBOutlet UITableView *tblView;
    int reloadTo;
    NSString *strFirstName, *strLastName, *strEmailId, *strDisplayName, *strNewPswd, *strConfirmPwsd, *strPhnNumber, *strAbtMe, *strTimezone;
    
    BOOL isSelected;
    BOOL buttonClicked;
    BOOL agreedToTerms;

    UIImage *imgForButton;
    
    NSArray *arrTimeZones;
    
    UIPickerView *pickerTimezones;
    UIToolbar* keyboardDoneButtonView;

}
- (IBAction)cancelBtnClicked:(id)sender;
-(void)changeTheView;

@end
