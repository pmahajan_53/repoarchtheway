//
//  VerificationCntlr.m
//  Arch The Way
//
//  Created by Abhishek Pednekar on 26/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import "VerificationCntlr.h"
#import "CommonForServices.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"


@interface VerificationCntlr ()

@end

@implementation VerificationCntlr

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    txtMobileNo.layer.borderWidth = 2.0;
    txtMobileNo.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    txtConfirmationCode.layer.borderWidth = 2.0;
    txtConfirmationCode.layer.borderColor = [UIColor lightGrayColor].CGColor;


    

    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated
{
    [self sendSMSToUser];
}

-(void)sendSMSToUser
{
    id response = [[NSUserDefaults standardUserDefaults] objectForKey:@"RegisterResponse"];
    
    response   = [response stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    id dictionary = [CommonForServices dictionaryFromOAuthResponseString:response];
    
    if ([dictionary isKindOfClass:[NSDictionary class]])
    {
        id reset_code = [dictionary objectForKey:@"reset_code"];
        id phone_number = [dictionary objectForKey:@"phone_number"];
        id tid = [dictionary objectForKey:@"tid"];
        
        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:tid,@"tid",phone_number,@"phone_number",reset_code,@"reset_code", nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:dic forKey:@"ConfirmationDic"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString *url = [NSString stringWithFormat:@"http://smsc.ssexpertsystem.com/api/web2sms.php?workingkey=15466knav929hz021lta&to=%@&sender=ARCHTW&message=Your confirmation code for arch is %@",phone_number,reset_code];
        
        NSString *newString = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [CommonForServices sendSmsToUsesr:newString];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    id dictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"ConfirmationDic"];
    
    if ([dictionary isKindOfClass:[NSDictionary class]])
    {
        txtMobileNo.text = [dictionary objectForKey:@"phone_number"];
//        txtConfirmationCode.text = [dictionary objectForKey:@"reset_code"];
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (IBAction)btnConfirmClicked:(id)sender
{
    [SVProgressHUD show];
    id dictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"ConfirmationDic"];
    [CommonForServices verifyMethod:dictionary];
}

- (IBAction)btnResendCodeClicked:(id)sender
{
    
    [self sendSMSToUser];
    
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UITabBarController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"tabbarcontroller"];
    [tabBarController setViewControllers:[NSArray arrayWithObjects:
    [storyboard instantiateViewControllerWithIdentifier:@"HomeNav"],
    [storyboard instantiateViewControllerWithIdentifier:@"ClassesNav"],
    [storyboard instantiateViewControllerWithIdentifier:@"HistoryNav"],
     [storyboard instantiateViewControllerWithIdentifier:@"SettingsNav"],nil]];
    
    AppDelegate *appObj = (AppDelegate *)[UIApplication sharedApplication].delegate;
    tabBarController.selectedIndex = 0;
    appObj.window.rootViewController = tabBarController;*/

}

- (IBAction)cancelBtnClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
