//
//  ClassCntlr.h
//  Arch The Way
//
//  Created by Amit Naik on 08/07/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ClassCntlr : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *arrCheck;
    
}

@property (nonatomic, strong) NSMutableDictionary *dicTopass;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;

@end
