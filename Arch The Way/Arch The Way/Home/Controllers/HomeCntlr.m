//
//  HomeCntlr.m
//  Arch The Way
//
//  Created by Ashissh Raichura on 27/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import "HomeCntlr.h"
#import "CommonForServices.h"
#import "SVProgressHUD.h"
#import "ClassCntlr.h"
#import "Constants.h"
#import "AppDelegate.h"

#define IS_IPHONE_4 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)480) < DBL_EPSILON)
#define IS_IPHONE_5 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)568) < DBL_EPSILON)
#define IS_IPHONE_6 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)


@interface HomeCntlr ()

@end

@implementation HomeCntlr

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [SVProgressHUD dismiss];
    self.navigationItem.title = @"Arch The Way";
    
    viewForTextview.layer.borderColor = [UIColor grayColor].CGColor;
    viewForTextview.layer.borderWidth = 1.0;
    
    txtView.text = @"Send study tips,motivational messages, emergency updates or simply as your students how they liked the class.";
    txtView.textColor = [UIColor lightGrayColor]; //optional
    UIToolbar *phoneNumberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width,30)];
    phoneNumberToolbar.barStyle = UIBarStyleDefault;
    phoneNumberToolbar.items = [NSArray arrayWithObjects:
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneBtnClicked)],
                                nil];
    [phoneNumberToolbar sizeToFit];
    txtView.inputAccessoryView = phoneNumberToolbar;
    
    isSelected = NO;
    
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectZero];
    
    datePicker.minimumDate = [NSDate date];
    
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    
    datePicker.minuteInterval = 5;
    
    datePicker.backgroundColor = [UIColor whiteColor];
    
    txtField.inputView = datePicker;
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)];
    
    btnDone.tintColor = [UIColor whiteColor];
    
    UIToolbar *toolBarDone = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width,44)];
    
    toolBarDone.barStyle = UIBarStyleBlackOpaque;
    toolBarDone.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                         
                         btnDone,
                         
                         nil];
    
    [toolBarDone sizeToFit];
    
    txtField.inputAccessoryView = toolBarDone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postedSuccessfully) name:@"PostedSucessfully" object:nil];
    APP_DELEGATE.dic = nil;

    
    id response = [[NSUserDefaults standardUserDefaults] objectForKey:@"LoginResponse"];
    
    response   = [response stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    id dictionary = [CommonForServices dictionaryFromOAuthResponseString:response];

    NSString *strDisplayName = [dictionary objectForKey:@"display_name"];
    if ([strDisplayName length]!=0)
    {
        self.navigationItem.title = strDisplayName;//@"Arch The Way";
    }
    
    self.view.frame = [UIScreen mainScreen].bounds;
    
//    scrollView_.canCancelContentTouches = YES;
//    scrollView_.delaysContentTouches = YES;
//    [scrollView_ setDelaysContentTouches:NO];

    [btnSelectClass addTarget:self action:@selector(btnSelectClass:) forControlEvents:UIControlEventTouchUpInside];
    [btnSend addTarget:self action:@selector(btnSendClicked:) forControlEvents:UIControlEventTouchUpInside];

}

- (void)viewDidLayoutSubviews
{
    if (IS_IPHONE_4)
    {
        scrollView_.contentSize =CGSizeMake(self.view.frame.size.width, 400);
    }
}


-(void)doneWithNumberPad
{
    [txtField resignFirstResponder];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"dd-MMM-YYYY HH:mm:ss"];
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    
    
    
    
    NSDate *pickedDOB = [datePicker date];
    NSString *DOB = [dateFormatter stringFromDate:pickedDOB];
    
    [[NSUserDefaults standardUserDefaults] setObject:DOB forKey:@"scheduleTime"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog (@"This is DOB %@", DOB);
    
    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm"];
    
    
    lblDate.text = [NSString stringWithFormat:@" %@",[dateFormatter stringFromDate:pickedDOB]];
}



#pragma mark - Textview delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Send study tips,motivational messages, emergency updates or simply as your students how they liked the class."])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""])
    {
        textView.text = @"Send study tips,motivational messages, emergency updates or simply as your students how they liked the class.";
        textView.textColor = [UIColor lightGrayColor];
    }
    [textView resignFirstResponder];
}

-(void)doneBtnClicked
{
    [txtView resignFirstResponder];
}


- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    return NO;
}

#pragma mark - Button Clicks
- (IBAction)btnTouchHereClicked:(id)sender
{
    UIActionSheet* myAction = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose existing", nil];
    [myAction showInView:self.view];
}

- (IBAction)btnTickUntick:(id)sender
{
    if (isSelected)
    {
        [txtField resignFirstResponder];
        [btnTickUntick setImage:[UIImage imageNamed:@"UnCheck"] forState:UIControlStateNormal];
        isSelected = NO;
        lblDate.text = @"Check To Send It Later";
    }
    else
    {
        [txtField becomeFirstResponder];
        [btnTickUntick setImage:[UIImage imageNamed:@"Check"] forState:UIControlStateNormal];
        isSelected= YES;
    }
}

-(void)postedSuccessfully
{
    isSelected = NO;
    [txtField resignFirstResponder];
    [btnTickUntick setImage:[UIImage imageNamed:@"UnCheck"] forState:UIControlStateNormal];
    lblDate.text = @"Check To Send It Later";

    txtView.text = @"";
    
    lblFileName.text = @"Touch Here To Send Files";
    APP_DELEGATE.dic = nil;
}

- (IBAction)btnInviteStudentsClicked:(id)sender
{
    
}

- (IBAction)btnSendClicked:(id)sender
{
    if (([txtView.text isEqualToString:@"Send study tips,motivational messages, emergency updates or simply as your students how they liked the class."])||([txtView.text isEqualToString:@""]))
    {
        UIAlertView *alertvw = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please enter the message, to be posted" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertvw show];
        return;
    }
    if (APP_DELEGATE.dic==nil)
    {
        UIAlertView *alertvw = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Please select the classes" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertvw show];
        return;

    }
    [SVProgressHUD show];
    
    NSLog(@"Dic to Pass - %@",APP_DELEGATE.dic);
    
    NSString *body = txtView.text;
    [APP_DELEGATE.dic setObject:body forKey:@"body"];
    [APP_DELEGATE.dic setObject:@"" forKey:@"timezone"];
    [APP_DELEGATE.dic setObject:@"" forKey:@"timestamp"];
    [APP_DELEGATE.dic setObject:@"" forKey:@"stimestamp"];
    [APP_DELEGATE.dic setObject:@"text" forKey:@"type"];
    [APP_DELEGATE.dic removeObjectForKey:@"topic_title"];
    
    
    [CommonForServices messageSendingAPI:APP_DELEGATE.dic];
    
    /*if ([APP_DELEGATE.dic isKindOfClass:[NSDictionary class]])
    {
        NSString *batch_Code = [APP_DELEGATE.dic objectForKey:@"batch_code"];
        NSString *size = [APP_DELEGATE.dic objectForKey:@"size"];
        NSString *stimestamp = [APP_DELEGATE.dic objectForKey:@"stimestamp"];
        NSString *tid = [APP_DELEGATE.dic objectForKey:@"tid"];
        NSString *timestamp = [APP_DELEGATE.dic objectForKey:@"timestamp"];
        NSString *timezone = [APP_DELEGATE.dic objectForKey:@"timezone"];
        NSString *topic_title = [APP_DELEGATE.dic objectForKey:@"topic_title"];
        NSString *type = [APP_DELEGATE.dic objectForKey:@"type"];
        
        
        
        NSMutableString *requestString = [[NSMutableString alloc] init];
        [requestString appendFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@&%@=%@",@"batch_code",batch_Code,@"size",size,@"stimestamp",stimestamp,@"tid",tid,@"timestamp",timestamp,@"timezone",@"",@"type",@"text",@"body",body];
        
        [CommonForServices sendScheduleAPI:requestString];
    }*/

}

- (IBAction)btnSelectClass:(id)sender
{
    
    id response = [[NSUserDefaults standardUserDefaults] objectForKey:@"LoginResponse"];
    
    response   = [response stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    id dictionary = [CommonForServices dictionaryFromOAuthResponseString:response];
    NSMutableDictionary *dictionary1;
    if ([dictionary isKindOfClass:[NSDictionary class]])
    {
        id tid = [dictionary objectForKey:@"tid"];
        id batch_code = [dictionary objectForKey:@"batch_code"];
        id timezone = [dictionary objectForKey:@"timezone"];
        //id timezone = @"UTC/GMT +06:00 - Asia/Dhaka";
        id title = [dictionary objectForKey:@"title"];
        
//        display_name
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
        
        NSDate *date = [NSDate date];
        NSString *currentDate = [dateFormatter stringFromDate:date];
        
        NSString *scheduleDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"scheduleTime"];
        
        if ([[CommonForServices isString:scheduleDate] isEqualToString:@""])
        {
            scheduleDate = currentDate;
        }
        
        dictionary1 = [[NSMutableDictionary alloc] initWithObjectsAndKeys:tid,@"tid",batch_code,@"batch_code",@"1.0",@"size",@"Message",@"type",txtView.text,@"body",currentDate,@"timestamp",scheduleDate,@"stimestamp",timezone,@"timezone",title,@"topic_title", nil];
    }
    
    ClassCntlr *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ClassCntlr"];
    viewController.dicTopass = dictionary1;
    [self presentViewController:viewController animated:YES completion:nil];
    
}


#pragma mark - Image Picker Delegate & Related

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    imageToSend = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    [self dismissViewControllerAnimated:YES completion:nil];
    

    
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Document selected" message:@"Please name your selected file." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    alert.alertViewStyle=UIAlertViewStylePlainTextInput;
    [alert show];

}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    if (buttonIndex==0)
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        }
        else
        {
            UIAlertView *noCameraAlert = [[UIAlertView alloc] initWithTitle:@"Sorry !" message:@"Camera Not Available." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [noCameraAlert show];
            
        }
    }
    else if (buttonIndex==1)
    {
        //picker.mediaTypes = [NSArray arrayWithObject:kUTTypeVideo];
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self presentViewController:picker animated:YES completion:nil];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex == 0)
    {
        UITextField *textfield =  [alertView textFieldAtIndex: 0];
        if ([textfield.text length]==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Document selected" message:@"Please name your selected file." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            
            alert.alertViewStyle=UIAlertViewStylePlainTextInput;
            [alert show];
        }
        else
        {
            lblFileName.text = textfield.text;
        }
    }
}


@end
