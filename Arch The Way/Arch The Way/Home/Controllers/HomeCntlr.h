//
//  HomeCntlr.h
//  Arch The Way
//
//  Created by Ashissh Raichura on 27/06/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCntlr : UIViewController <UITextViewDelegate, UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIAlertViewDelegate,UITextViewDelegate>
{
    __weak IBOutlet UIView *topView;

    __weak IBOutlet UILabel *lblSubscriberCount;

    __weak IBOutlet UILabel *lblFeedbackCount;
    __weak IBOutlet UIButton *btnInviteStudents;
    
    
    __weak IBOutlet UIView *viewForTextview;
    __weak IBOutlet UIScrollView *scrollView_;
    __weak IBOutlet UITextView *txtView;
    UIImage *imageToSend;
    
    __weak IBOutlet UILabel *lblFileName;
    
    BOOL isSelected;
    
    
    __weak IBOutlet UIButton *btnTickUntick;
    __weak IBOutlet UITextField *txtField;
    
    UIDatePicker *datePicker;
    UIToolbar* keyboardDoneButtonView;
    
    __weak IBOutlet UILabel *lblDate;
    __weak IBOutlet UIButton *btnSend;
    __weak IBOutlet UIButton *btnSelectClass;
}



- (IBAction)btnTouchHereClicked:(id)sender;
- (IBAction)btnTickUntick:(id)sender;

- (IBAction)btnInviteStudentsClicked:(id)sender;
- (IBAction)btnSendClicked:(id)sender;
- (IBAction)btnSelectClass:(id)sender;
@end
