//
//  ClassCntlr.m
//  Arch The Way
//
//  Created by Amit Naik on 08/07/15.
//  Copyright (c) 2015 Abhishek Pednekar. All rights reserved.
// 4268

#import "ClassCntlr.h"
#import "CommonForServices.h"
#import "SVProgressHUD.h"
#import "HomeCntlr.h"
#import "AppDelegate.h"
#import "Constants.h"

@interface ClassCntlr ()
{
    NSMutableArray *selectedArray;
    NSMutableArray *selectedTitle;
    
    NSMutableArray *numberOfClasses;
    NSMutableArray *title;
    
}

@end

@implementation ClassCntlr

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    selectedArray = [[NSMutableArray alloc] init];
    selectedTitle = [[NSMutableArray alloc] init];
    numberOfClasses = [[NSMutableArray alloc] init];
    title = [[NSMutableArray alloc] init];

    NSArray *aa = [[NSArray alloc] init];
    NSArray *aa1 = [[NSArray alloc] init];
    
    //NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    //_dicTopass= [[pref objectForKey:@"dicTopass"] mutableCopy];
    NSString *className = [_dicTopass objectForKey:@"batch_code"];
        aa = [className  componentsSeparatedByString:@","];
    [numberOfClasses addObjectsFromArray:aa];
    
    
    NSString *titleName = [_dicTopass objectForKey:@"topic_title"];
    aa1 = [titleName componentsSeparatedByString:@","];
    [title addObjectsFromArray:aa1];
    
    arrCheck = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [title count] ; i++)
    {
        [arrCheck addObject:@"0"];
    }
    
    
}

#pragma mark - Tableview methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [numberOfClasses count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"classCell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@",[numberOfClasses objectAtIndex:indexPath.row]];
    
    
    
    if ([[arrCheck objectAtIndex:indexPath.row] isEqualToString:@"1"])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if ([[arrCheck objectAtIndex:indexPath.row] isEqualToString:@"1"])
    {
        [arrCheck replaceObjectAtIndex:indexPath.row withObject:@"0"];
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [selectedArray removeObject:[NSString stringWithFormat:@"%@",[numberOfClasses objectAtIndex:indexPath.row]]];
        [selectedTitle removeObject:[NSString stringWithFormat:@"%@",[title objectAtIndex:indexPath.row]]];

    }
    else
    {
        [arrCheck replaceObjectAtIndex:indexPath.row withObject:@"1"];

    
            [selectedArray addObject:[NSString stringWithFormat:@"%@",[numberOfClasses objectAtIndex:indexPath.row]]];
            [selectedTitle addObject:[NSString stringWithFormat:@"%@",[title objectAtIndex:indexPath.row]]];

    }
    
    [tableView reloadData];
}

- (IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)done:(id)sender
{
    if ([selectedArray count]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please select one of the class" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    NSString *sizeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[selectedArray count]];
    
    NSString *myString = [selectedArray componentsJoinedByString:@","];
     NSString *myTitle = [selectedTitle componentsJoinedByString:@","];
    
    [_dicTopass setValue:myString forKey:@"batch_code"];
    [_dicTopass setValue:sizeValue forKey:@"size"];
    [_dicTopass setValue:myTitle forKey:@"topic_title"];
    [_dicTopass setValue:@"schedule" forKey:@"type"];
    
    APP_DELEGATE.dic = [_dicTopass mutableCopy];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
